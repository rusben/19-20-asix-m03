public class Element {
  int id;
  String content;

  Element(int id, String content) {
    this.id = id;
    this.content = content;
  }

  public String toString() {
    return "[Element]"+" id: "+id+" content: "+content;
  }

}
