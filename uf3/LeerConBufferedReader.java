
import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class LeerConBufferedReader {
  public static void main(String[] args) {

    try {
      BufferedReader inputStream = new BufferedReader(new FileReader(new File("EscribirConFileWriter.java")));
      String line = "";

      while((line = inputStream.readLine()) != null) {
        System.out.println(line);
      }

      inputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }

  }
}
