package net.xeill.elpuig;

// https://www.tutorialspoint.com/jdbc/jdbc-create-database.htm
// https://dev.mysql.com/doc/connector-j/8.0/en/connector-j-binary-installation.html

//STEP 1. Import required packages
import java.sql.*;
import java.util.*;
import java.io.*;

class DBManager {

  boolean DEBUG_MODE  = true;
  final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
  final String DB_URL = "jdbc:mysql://localhost/";
  final String DB_NAME = "fruteria";

  //  Database credentials
  final String USER = "root";
  final String PASS = "password";

  Connection conn = null;
  Statement stmt = null;

  public boolean connect() {
    try{
      //STEP 2: Register JDBC driver
      Class.forName("com.mysql.jdbc.Driver");
      //STEP 3: Open a connection
      System.out.println("Connecting to database...");
      conn = DriverManager.getConnection(DB_URL+DB_NAME, USER, PASS);
      return true;
    } catch (Exception e){
       //Handle errors for JDBC
       e.printStackTrace();
       return false;
    }
  }

  public void close() {
    try {
      conn.close();
      System.out.println("Goodbye!");
    } catch (Exception e){
       e.printStackTrace();
    }
  }

  public ResultSet query(String sql) {
    try {
      //STEP 4: Execute a query
      if (DEBUG_MODE) {
        System.out.println("Creating statement...");
      }

      stmt = conn.createStatement();
      ResultSet rs = stmt.executeQuery(sql);

      if (DEBUG_MODE) {
        System.out.println("Query successfully executed...");
        System.out.println("[CONSULTA]");
        System.out.println(sql);
      }

      stmt.close();
      return rs;

    } catch(SQLException se){
       //Handle errors for JDBC
       se.printStackTrace();
       return null;
    }

  }

  void insertFruit(Fruta f) {

    String DBTable = "Fruta";

    try {
      String sql = "INSERT INTO "+DBTable+" (nombre, variedad, cantidad, precioKilo) VALUES (?,?,?,?)";
      PreparedStatement ps = conn.prepareStatement(sql);

      ps.setString(1, f.nombre);
      ps.setString(2, f.variedad);
      ps.setFloat(3, f.cantidad);
      ps.setFloat(4, f.precioKilo);

      System.out.println("[INSERT] Fruit "+f.nombre+" inserted");

      ps.executeUpdate();
    } catch(Exception e){
       e.printStackTrace();
    }
  }

}
