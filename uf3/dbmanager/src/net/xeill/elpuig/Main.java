package net.xeill.elpuig;

import java.util.*;
import java.io.*;

public class Main {
  public static void main(String[] args)  throws IOException {

    DBManager dbmanager = new DBManager();
    Fruta f = new Fruta("Kiwi", "Gold", 10, 5);
    Fruta g = new Fruta("Pera", "Ercolini", 100, 5.25f);
    Fruta h = new Fruta("Manzana", "Golden", 200, 3.4f);
    Fruta i = new Fruta("Sandía", "SugarBaby", 1000, 3.3f);

    if (dbmanager.connect()) {
      dbmanager.insertFruit(f);
      dbmanager.insertFruit(g);
      dbmanager.insertFruit(h);
      dbmanager.insertFruit(i);
    }
    dbmanager.close();

  }
}
