CREATE TABLE `Fruta` (
  `nombre` text COLLATE utf8_unicode_ci NOT NULL,
  `variedad` text COLLATE utf8_unicode_ci NOT NULL,
  `cantidad` float NOT NULL,
  `precioKilo` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

