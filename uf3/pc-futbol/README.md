# pc-futbol
PC FUTBOL, Java Terminal

## How to compile?
```bash
# Go into the project source folder (src)
$ cd pc-futbol/src

# Compile the main class
$ javac net/xeill/elpuig/Main.java

# Run the main
$ java net.xeill.elpuig.Main
```
