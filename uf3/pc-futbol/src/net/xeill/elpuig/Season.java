package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Stock
*
* @author Adrià
*
*/

public class Season {

  static ArrayList<Team> teams = new ArrayList<Team>();
  static ArrayList<Player> players = new ArrayList<Player>();

  Season() {
    initSeason();
  }

  public void initSeason() {

    // Equipos
    teams.add(new Team(1, "Barça", "Més que un club", 1899));

    // Jugadores
    players.add(new Player(10, "Lionel Messi", 1));

  }


  public void imprimirJugadores() {
    for (Player p : players) {
       p.print();
    }
  }

  public void imprimirJugadoresEquipo(int idTeam) {

  }

  public void anadirJugador() {

  }

  public static String getTeamName(int idTeam) {
    for (Team t : teams) {
       if (t.id == idTeam) return t.name;
    }
    return "No encontrado!";
  }


}
