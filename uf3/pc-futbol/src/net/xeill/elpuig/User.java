package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the User
*
* @author Adrià
*
*/

public class User {

  int id;
  String name;
  String surname;
  String email;

  User(int id, String name, String surname, String email) {
    this.id = id;
    this.name = name;
    this.surname = surname;
    this.email = email;
  }

  boolean buyGame(Game g) {
    return false;
  }

  boolean buyConsole(Console c) {
    return false;
  }

}
