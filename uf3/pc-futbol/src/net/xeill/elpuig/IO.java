package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the IO - Input/Output
*
* @author David
*
*/

public class IO {

  Scanner scanner = new Scanner(System.in);

  /**
  * Clears the screen.
  */
  void cls() {
    System.out.print("\033\143");
  }

  public void welcome() {
    System.out.println("------- Welcome to PC FUTBOL Terminal -------");
    System.out.println("1 - Jugadores");
    System.out.println("2 - Equipos");
    System.out.println("3 - Salir");
  }

  public int getWelcome() {
    String soption;
    int option;
    try {
      soption = scanner.nextLine();
      option = Integer.parseInt(soption);
    } catch (Exception e) {
      cls();
      welcome();
       return getWelcome();
    }
    return option;
  }


  void jugadores() {
    System.out.println("------- Jugadores -------");
    System.out.println("1 - Mostrar jugadores");
    System.out.println("2 - Añadir jugador");
  }

  public int getJugadoresOpcion() {
    String soption;
    int option;
    try {
      soption = scanner.nextLine();
      option = Integer.parseInt(soption);
    } catch (Exception e) {
      cls();
      jugadores();
       return getJugadoresOpcion();
    }
    return option;
  }


  void equipos() { }
  void salir() { }


}
