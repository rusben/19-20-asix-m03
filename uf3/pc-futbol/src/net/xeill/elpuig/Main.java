package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  David
*/
public class Main {
  public static void main(String[] args) {

    IO io = new IO();
    Season season = new Season();

    while (true) {
      io.welcome();
      int option = io.getWelcome();

      switch(option) {
        case 1: io.jugadores();
          int op = io.getJugadoresOpcion();

          switch(op) {
            case 1: season.imprimirJugadores();
            break;
            case 2: season.anadirJugador();
            break;
          }

          break;
        case 2: io.equipos();
          break;
        case 3: io.salir();
          break;
        default:
          break;

      }
    }
  }
}
