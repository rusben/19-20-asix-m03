package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the Team
*
* @author Adrià
*
*/

public class Team {

  int id;
  String name;
  String description;
  int foundation; // año de fundación

  Team(int id, String name, String description, int foundation) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.foundation = foundation;
  }

  public void print() {
    System.out.println("id: "+id);
    System.out.println("name: "+name);
    System.out.println("description: "+description);
    System.out.println("foundation: "+foundation);
  }

}
