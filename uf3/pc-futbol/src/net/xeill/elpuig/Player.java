package net.xeill.elpuig;

import java.io.*;
import java.util.*;

/**
* This class implements the User
*
* @author Adrià
*
*/

public class Player {

  int dorsal;
  String name;
  int idTeam;

  Player(int dorsal, String name, int idTeam) {
    this.dorsal = dorsal;
    this.name = name;
    this.idTeam = idTeam;
  }


  public void print() {
    System.out.println("dorsal: "+dorsal);
    System.out.println("name: "+name);
    System.out.println("id: "+idTeam);
  }

}
