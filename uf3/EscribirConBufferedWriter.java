
import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class EscribirConBufferedWriter {
  public static void main(String[] args) {

    try {
      BufferedWriter outputStream = new BufferedWriter(new FileWriter(new File("escrituras"), true)); // append

      for (int i = 1; i <= 10; i++) {
        outputStream.write("Esta es la línea escrita con el BufferedWriter "+i+"\n");
      }

      outputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }

  }
}
