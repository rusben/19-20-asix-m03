package net.xeill.elpuig;

import java.util.*;
import java.io.*;

public class Main {
  public static void main(String[] args)  throws IOException {
    Fruteria puig = new Fruteria();

    puig.inicializarStock(new File("net/xeill/elpuig/frutas.data"));
    puig.mostrarStock();

    // interacciones de los usuarios
    // puig.stock.add(new Fruta("melón", "canario", 200.0f, 2.99f));

    puig.guardarStock(new File("net/xeill/elpuig/frutas.data"));
  }
}
