package net.xeill.elpuig;

class Fruta​ {
  String nombre;
  String variedad;
  float cantidad; // Kilogramos
  float precioKilo;

  Fruta(String nombre, String variedad, float cantidad, float precioKilo) {
    this.nombre = nombre;
    this.variedad = variedad;
    this.cantidad = cantidad;
    this.precioKilo = precioKilo;
  }
}
