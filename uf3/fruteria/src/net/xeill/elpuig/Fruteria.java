package net.xeill.elpuig;

import java.util.*;
import java.io.*;

class Fruteria​ {

  ArrayList<Fruta> stock = new ArrayList<Fruta>();

  void mostrarStock() {
    for (Fruta f : stock) {
      System.out.println("Fruta: "+f.nombre+" v. "+f.variedad);
      System.out.println("Cantidad: "+f.cantidad+" a un precio de "+f.precioKilo+"€/kg");
    }
  }

  void guardarStock(File file) {
    try {
      BufferedWriter outputStream = new BufferedWriter(new FileWriter(file, false)); // NO append

      for (Fruta f : stock) {
        outputStream.write(f.nombre+":"+f.variedad+":"+f.cantidad+":"+f.precioKilo+"\n");
      }

      outputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }
  }

  void inicializarStock(File file) {

    try {
      BufferedReader inputStream = new BufferedReader(new FileReader(file));
      String line = "";

      while((line = inputStream.readLine()) != null) {
        //System.out.println(line);
        String[] parts = line.split(":");
        // pera --> parts[0]
        // ercolini --> parts[1]
        // 200 --> parts[2]
        // 1.20 --> parts[3]

        // Creamos la fruta
        //Fruta(String nombre, String variedad, float cantidad, float precioKilo)
        Fruta f = new Fruta(parts[0], parts[1], Float.parseFloat(parts[2]), Float.parseFloat(parts[3]));
        // System.out.println(parts[0]);
        // System.out.println(parts[1]);
        // System.out.println(parts[2]);
        // System.out.println(parts[3]);

        stock.add(f);
      }

      inputStream.close();

    } catch(Exception e) {
      System.out.println(e.getMessage());
    }

  }

}
