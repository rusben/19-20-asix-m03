import java.io.*;
import java.util.*;

class LeerFrutas {

  public static void main(String[] args)  throws IOException {
    ArrayList<Fruta> cesta = new ArrayList<Fruta>();

    try {

      BufferedReader inputStream = new BufferedReader(new FileReader(new File("frutas.in")));
      String line;
      int i = 0;
      while((line = inputStream.readLine()) != null) {
            String[] values = line.split(":");

            // Creamos la Fruta
            Fruta fruta = new Fruta(values[0], values[1], Integer.parseInt(values[2]));
            cesta.add(fruta);
      }
      inputStream.close();
    } catch(Exception e) {
      System.out.println(e.getMessage());
    }


    System.out.println("Comprobación");
    // Comprobación
    for (Fruta f : cesta) {
      System.out.println(f.nombre);
      System.out.println(f.color);
      System.out.println(f.gramos);
    }


  }

}

class Fruta​ {
  String nombre;
  String color;
  int​ gramos;

  Fruta(String n, String c, int g) {
    this.nombre = n;
    this.color = c;
    this.gramos = g;
  }
}
