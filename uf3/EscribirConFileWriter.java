
import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* @author  ASIX1
*/
public class EscribirConFileWriter {
  public static void main(String[] args) {

    try {

      FileWriter fw = new FileWriter(new File("escribir"), true); // append

      for (int i = 1; i <= 10; i++) {
        fw.write("Esta es la línea "+i+"\n");
      }

      fw.close();

    } catch (Exception e) {
      System.out.println(e.getMessage());
    }

  }
}
