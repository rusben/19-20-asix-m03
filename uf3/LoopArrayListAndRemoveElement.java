import java.util.ArrayList;

public class LoopArrayListAndRemoveElement {

  static ArrayList<Element> elements = new ArrayList<Element>();

  public static void main(String[] args) {

    // Añadimos 5 elementos
    addElements();
    // Mostramos los elementos
    printElements();
    // Eliminamos el elementos con id 3
    removeElement(3);
    // Mostramos los elementos
    printElements();

  }


  public static boolean removeElement(int id) {
    // Hacemos una copia para iterar el ArrayList
    // Si encontramos el elemento con ese id lo eliminamos
    // en el arraylist de elements
    // Recorremos la copia y eliminamos en el arraylist original
    for (Element e : new ArrayList<Element>(elements)) {
      if (e.id == id){
        elements.remove(e);
        System.out.println("================================================================================");
        System.out.println("[ELEMENT DELETED] id:"+id);
        System.out.println("================================================================================");

        return true;
      }
    }
    return false;
  }

  public static void addElements() {
    elements.add(new Element(1, "Elemento 1"));
    elements.add(new Element(2, "Elemento 2"));
    elements.add(new Element(3, "Elemento 3"));
    elements.add(new Element(4, "Elemento 4"));
    elements.add(new Element(5, "Elemento 5"));
  }

  public static  void printElements() {
    System.out.println("================================================================================");
    // Imprimimos el array de elementos
    for (Element e : elements) {
      System.out.println(e);
    }
    System.out.println("================================================================================");
  }
}
