# Bomberman



## Key Features

* Destroy enemies with no effort.
  - Instantly see how the board is changing.
* Enemies AI improved.
* Javadoc documentation available.
* A huge community behind the back.



## How to generate javadoc?

From the project root folder write the folowing command to generate the javadoc

  javadoc -d doc/elpuig/ -sourcepath src/ -subpackages net

### [FAQ] Generate javadoc from command line:

https://stackoverflow.com/questions/4592396/how-to-generate-javadoc-from-command-line
