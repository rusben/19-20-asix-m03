package net.xeill.elpuig;

import java.util.*;
import java.io.*;

/**
* This class implements the Main.
*
* <p>The methods of this class all throw a <tt>NullPointerException</tt>
* if the collections or class objects provided to them are null.
*
*
* @author  Rusben
*/
public class Main {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    System.out.println("Hello world!");
    Board b = new Board(10, 10);
    b.print();


    input.nextInt();

    b.clear();


    Player p = new Player(0, 0, "Vinicius");
    Enemy e = new Enemy(0, 2, "Gerard");
    Bomb a = new Bomb(4,5, "MOAB");

    b.addAvatar(p, p.x, p.y);
    b.addAvatar(e, e.x, e.y);
    b.addAvatar(a, a.x, a.y);

    b.print();
  }
}
