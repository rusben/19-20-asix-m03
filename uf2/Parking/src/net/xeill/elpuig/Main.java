package net.xeill.elpuig;

public class Main {

    public static void main(String[] args) {

        Parking p = new Parking(10);
        p.status();

        Car a = new Car("8934XSN");
        Car b = new Car("3454CDS");
        Car c = new Car("6245BXN");

        p.aparcar(a, 1);
        p.aparcar(b, 11);
        p.aparcar(c, 1);
        p.aparcar(b, 2);
        p.aparcar(c, 3);

        p.status();

    }
}
