package net.xeill.elpuig;

public class Car {

    // La matrícula del coche
    private String plate;

    // Constructor
    public Car(String plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        return this.plate;
    }
}
