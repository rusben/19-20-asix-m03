package net.xeill.elpuig;

public class Parking {

    private Car[] places;

    // Constructor de parking con número de plazas
    public Parking (int nPlaces) {
        this.places = new Car[nPlaces];
    }

    public void status() {
        for (int i = 0; i < this.places.length; i++) {
            System.out.println(i+":");
            System.out.println(this.places[i]);
        }
    }

    public boolean aparcar(Car romato, int nPlaza) {

        if (this.isEmpty(nPlaza)) {
            // Aparcar el Car romato en la plaza nPlaza
            this.places[nPlaza] = romato;
            return true;
        } else {
            // No he podido aparcar
            System.out.println("PLAZA OCUPADA");
            return false;
        }
    }

    // Nos dice si una plaza con numeroPlaza está
    // vacía (true)
    public boolean isEmpty(int numeroPlaza) {

        // Comprobar si el número de plaza está dentro de nuestro rango de plazas
        if (numeroPlaza >= 0 && numeroPlaza < this.places.length) {

            if (this.places[numeroPlaza] instanceof Car) {
                // La plaza no está vacía
                return false;
            } else {
                return true;
            }
        }

        // En el caso que el número de plaza no exista en mi parking
        return false;
    }

}
