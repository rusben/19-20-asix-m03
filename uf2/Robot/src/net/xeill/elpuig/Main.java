package net.xeill.elpuig;

public class Main {

    public static void main(String[] args) {

      Robot r1 = new Robot("Reuno");
      Robot r2 = new Robot("Arturito");

      r1.saluda();
      r2.saluda();

      System.out.println(r1.calcula(2,3,"SUMAR"));
      System.out.println(r2.calcula(2,3,"MULTIPLICAR"));

      System.out.println(r1);
      System.out.println(r2);

    }
}
