package net.xeill.elpuig;

class Robot {

  // Atributos
  String name;

  // Constructor
  Robot(String robotName) {
    this.name = robotName;
  }

  // Métodos
  public void saluda() {
    System.out.println("Hola me llamo "+this.name);
  }

  /*
    operation: SUMAR, RESTAR, MULTIPLICAR, DIVIDIR
  */
  public int calcula(int a, int b, String operation) {

    if (operation == "SUMAR") return a+b;
    if (operation == "RESTAR") return a-b;
    if (operation == "MULTIPLICAR") return a*b;
    if (operation == "DIVIDIR" && b != 0) return a/b;

    return 0;

  }

  @Override
  public String toString() {
      return this.name;
  }

}
