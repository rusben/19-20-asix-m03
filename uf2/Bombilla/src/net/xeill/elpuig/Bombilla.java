package net.xeill.elpuig;

public class Bombilla {

    // atributos de clase o atributos de instancia
    private boolean isOn;
    private String name;

    public Bombilla(boolean initialState, String initialName) {
        this.isOn = initialState;
        this.name = initialName;
    }

    // cambiar nombre
    public void changeName(String newName) {
        this.name = newName;
    }

    // Enciende la bombilla
    public void switchOn() {
        this.isOn = true;
    }

    // Apaga la bombilla
    public void switchOff() {
        this.isOn = false;
    }

    public void status() {
        System.out.println("La bombilla "+this.name+" está "
                +(isOn ? "encendida" : "apagada")+".");
    }
}

