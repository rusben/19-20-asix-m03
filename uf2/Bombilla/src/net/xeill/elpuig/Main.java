package net.xeill.elpuig;

public class Main {

    public static void main(String[] args) {

        Bombilla b = new Bombilla(true, "Pepe");
        b.status();

        Bombilla b2 = new Bombilla(false, "Toni");
        b2.status();

        // Crea el array de bombillas
        Bombilla[] focos = new Bombilla[5];

        // Crea la bombilla de la posición 0 del array
        focos[0] = new Bombilla(true, "Cristian");

        // Crea las bombillas de 1-4 del array
        for (int i = 1; i < focos.length; i++) {
            focos[i] = new Bombilla(true, "Bombilla"+i);
        }

        // Imprime el estado de las bombillas
        for (int i = 0; i < focos.length; i++) {
            focos[i].status();
        }
    }
}
