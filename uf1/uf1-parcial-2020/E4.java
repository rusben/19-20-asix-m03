import java.io.*;
import java.util.*;

public class E4 {

    public static void main(String[] args) {
      Scanner input = new Scanner(System.in);

      int n = input.nextInt();

      while (n != -2) {

        if (n == -1) {
          System.out.println();
        } else if (n >= 0) {
          // Imprimir n espacios y una almohadilla
          for (int i = 0; i < n; i++) {
            System.out.print(" ");
          }
          System.out.print("#");
        } else {
            // Para los casos con n < -2
        }

        n = input.nextInt();

      }

    }
  }
