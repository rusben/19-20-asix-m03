import java.io.*;
import java.util.*;

public class E3 {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int cantidad = input.nextInt();

      int m500 = 0;
      int m100 = 0;
      int m50 = 0;
      int m5 = 0;
      int m1 = 0;

      while (cantidad > 0) {

        if (cantidad >= 500) {
          cantidad = cantidad - 500;
          m500++;
        } else if (cantidad >= 100) {
          cantidad = cantidad - 100;
          m100++;
        } else if (cantidad >= 50) {
          cantidad = cantidad - 50;
          m50++;
        } else if (cantidad >= 5) {
          cantidad = cantidad - 5;
          m5++;
        } else {
          cantidad = cantidad - 1;
          m1++;
        }

      }

      if (m500 > 0) {
        System.out.print(m500+" moneda");
        if (m500 > 1) System.out.print("s");
        System.out.println(" de 500 Puixets");
      }

      if (m100 > 0) {
        System.out.print(m100+" moneda");
        if (m100 > 1) System.out.print("s");
        System.out.println(" de 100 Puixets");
      }

      if (m50 > 0) {
        System.out.print(m50+" moneda");
        if (m50 > 1) System.out.print("s");
        System.out.println(" de 50 Puixets");
      }

      if (m5 > 0) {
        System.out.print(m5+" moneda");
        if (m5 > 1) System.out.print("s");
        System.out.println(" de 5 Puixets");
      }

      if (m1 > 0) {
        System.out.print(m1+" moneda");
        if (m1 > 1) System.out.print("s");
        System.out.println(" de 1 Puixet");
      }


    }
}
