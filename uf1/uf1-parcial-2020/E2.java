import java.io.*;
import java.util.*;

public class E2 {

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    int comensales = input.nextInt();
    double precioKiloArroz = input.nextDouble();
    double precioKiloGambas = input.nextDouble();

    //double cantidadArroz = comensales * 0.5 / 4;
    //double cantidadGambas = comensales * 0.25 / 4;

    // Si a cada 4 le doy medio kilo arroz
    // a 1 le doy 0.125 kg de arroz
    double cantidadArroz = comensales * 0.125;
    // Si a cada 4 le doy 0.25 kilos gambas
    // a 1 le doy 0.0625 kg de gambas
    double cantidadGambas = comensales * 0.0625;

    double precioArroz = cantidadArroz * precioKiloArroz;
    double precioGambas = cantidadGambas * precioKiloGambas;

    double precioTotal = precioArroz + precioGambas;

    System.out.println(cantidadArroz+" kg arroz");
    System.out.println(cantidadGambas+" kg gambas");
    System.out.println(precioArroz+"€ arroz");
    System.out.println(precioGambas+"€ gambas");
    System.out.println("TOTAL: "+precioTotal+"€");

  }
}
