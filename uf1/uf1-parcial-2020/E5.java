import java.io.*;
import java.util.*;

public class E5 {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int c1 = input.nextInt();
      int c2 = input.nextInt();
      int c3 = input.nextInt();

      int a = input.nextInt();
      int b = input.nextInt();
      int c = input.nextInt();

      boolean abierta = false;

      while (c != -1) {
        if (c1 == a && c2 == b && c3 == c) { // ABIERTA
          abierta = true;
        }

        a = b;
        b = c;
        c = input.nextInt();
      }

      if (abierta == true) {
        System.out.println("ABIERTA");
      } else {
        System.out.println("CERRADA");
      }

    }
}
