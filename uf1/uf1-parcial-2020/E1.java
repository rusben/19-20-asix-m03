import java.io.*;
import java.util.*;

public class E1 {

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    int n = input.nextInt();

    int contador = 0;
    int sumaPares = 0;
    int productoImpares = 1;

    while (n != -1) {

      if (n % 2 == 0) { // Es par
        sumaPares = sumaPares + n;
      } else {
        productoImpares = productoImpares * n;
      }

      contador++;

      n = input.nextInt();
    }

    System.out.println(sumaPares);
    System.out.println(productoImpares);
    System.out.println(contador);

  }
}
