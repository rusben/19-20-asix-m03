import java.io.*;
import java.util.*;

/*
1. Notes analytics.
Un Instituto de Santa Coloma de Gramenet en el que se estudia FP de Informática necesita unprograma en Java para saber de forma rápida si un alumno aprueba o suspende una UF a partir de lasnotas recogidas durante el transcurso de la misma.Supongamos que para todas las UFs tienen un total de 85 horas y los elementos a evaluar son siemprelos siguientes:-5 prácticas.-3 exámenes parciales.-1 examen final.-Faltas de asistencia.La nota mínima para aprobar una práctica es 5.La nota mínima para aprobar un examen es 5. Las notas están comprendidas entre 0 y 10, sin decimales.Los requisitos para aprobar mediante evaluación continua son:-Haber aprobado al menos las tres últimas prácticas.-No faltar a más del 20% de las horas de la UF.-Aprobar todos los exámenes parciales.También se puede aprobar simplemente aprobando el examen final, siempre y cuando no se hayafaltado a más del 20% de las horas de la UF.
*/

public class E1 {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);
      int p1 = input.nextInt();
      int p2 = input.nextInt();
      int p3 = input.nextInt();
      int p4 = input.nextInt();
      int p5 = input.nextInt();

      int e1 = input.nextInt();
      int e2 = input.nextInt();
      int e3 = input.nextInt();

      int ef = input.nextInt();

      int f = input.nextInt();


      // Aprueba por evaluacion continua
      if (p3 >= 5 && p4 >= 5 && p5 >= 5 && f <= 17 && e1 >= 5 && e2 >= 5 && e3 >= 5) {
        System.out.println(true);
      } else if (ef >= 5 && f <= 17) { // Aprueba por examen final
        System.out.println(true);
      } else {
        System.out.println(false);
      }

    }
}
