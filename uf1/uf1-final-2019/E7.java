import java.io.*;
import java.util.*;

public class E7 {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */

        Scanner in = new Scanner(System.in);
        String line = in.nextLine();

        // En last está el último carácter que hay que imprimir
        int last = line.length();

        // Posicionamos nuestro apuntador i al final de la palabra.
        for (int i = line.length()-1; i >= 0 ; i--) {
          // Si en la posición que miramos hay espacio
          if (line.charAt(i) == ' ') {

            for (int j=i+1; j<last && j < line.length(); j++) {
              System.out.print(line.charAt(j));
            }
            System.out.print(" ");

            last = i;
          }
        }

        // Printar el resto de la frase (la primera palabra)
        for (int i = 0; i < last; i++) {
          System.out.print(line.charAt(i));
        }

    }
}
