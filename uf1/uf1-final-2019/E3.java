import java.io.*;
import java.util.*;

/*
Sedeseacomprobarelfuncionamientodeungeneradordecorrienteeléctrica.Paraellosemideperiódicamente el voltaje de la corriente generada.Seconsideraqueelgeneradorfuncionacorrectamentesielvoltajealternasubidasybajadas.Esdecirsienunamediciónelvoltajesubió,enlasiguientedebehaberbajado,yviceversa.Paraquelacorrientesea correcta la primera medición de voltaje respecto a la segunda medición debe ser de subida.
*/

public class E3 {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int n = input.nextInt();
      int m = input.nextInt();

      int i  = 0;
      boolean correcte = true;

// 1 5 2 7 3  0

      while (m != 0) {

        if (i % 2 == 0) { // vuelta par
          if (m <= n) {
            correcte = false;
          }
        } else {  // vuelta impar
          if (n <= m) {
            correcte = false;
          }
        }

        n = m;
        m = input.nextInt();

        i++;
      }

      System.out.println(correcte ? "CORRECTO" : "INCORRECTO");

    }
}
