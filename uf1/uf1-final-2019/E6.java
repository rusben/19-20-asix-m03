import java.io.*;
import java.util.*;

/*
Ejercicio 6. Invertir el orden de las palabras.Dada una línea con varias palabras, invertir el orden de dichas palabras.
*/

public class E6 {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      String line = input.nextLine();

      String[] words = line.split(" ");

      for (int i = words.length - 1; i >= 0; i--) {
        System.out.print(words[i]+" ");
      }

    }


}
