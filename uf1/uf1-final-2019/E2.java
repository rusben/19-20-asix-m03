import java.io.*;
import java.util.*;

/*
Ejercicio 2. Estadísticas en el teatro.El gran teatro del Puig necesita tener estadísticas sobre cada uno de los grupos que asisten al teatro enun día en el que hay función (viernes, sábado y domingo).El registro de entrada de la sala guarda una serie de información de las personas que acceden, de esteregistro únicamente nos interesa la edad de esas personas, para en un futuro descubrir si existe algunarelación entre el día de la semana y la edad de las personas que acuden.A partir del registro de entrada se debe indicar cuántas personas en cada franja de edad han acudidocada día de la semana al teatro.La franjas de edad son las siguientes:-Menores de 18 años.-Entre 18 y 65 años (ambos incluídos)-Mayores de 65 años.
*/

public class E2 {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int n = input.nextInt();
      int age;

      int v017 = 0;
      int v1865 = 0;
      int v65 = 0;

      int s017 = 0;
      int s1865 = 0;
      int s65 = 0;

      int d017 = 0;
      int d1865 = 0;
      int d65 = 0;

      String day = "";

      for (int i = 0; i < n; i++) {

          day = input.next();
          age = input.nextInt();
          while (age != -1) {

            if (day.equals("viernes")) {
              if (age >= 0 && age <= 17) {
                v017++;
              } else if (age >= 18 && age <= 65) {
                v1865++;
              } else {
                v65++;
              }
            }

            if (day.equals("sabado")) {
              if (age >= 0 && age <= 17) {
                s017++;
              } else if (age >= 18 && age <= 65) {
                s1865++;
              } else {
                s65++;
              }
            }

            if (day.equals("domingo")) {
              if (age >= 0 && age <= 17) {
                d017++;
              } else if (age >= 18 && age <= 65) {
                d1865++;
              } else {
                d65++;
              }
            }

            age = input.nextInt();

          }

      }

      System.out.println("viernes");
      System.out.println("0-17 : "+v017);
      System.out.println("18-65: "+v1865);
      System.out.println("+65  : "+v65);
      System.out.println();
      System.out.println("sabado");
      System.out.println("0-17 : "+s017);
      System.out.println("18-65: "+s1865);
      System.out.println("+65  : "+s65);
      System.out.println();
      System.out.println("domingo");
      System.out.println("0-17 : "+d017);
      System.out.println("18-65: "+d1865);
      System.out.println("+65  : "+d65);

      /*
viernes
0-17 : 4
18-65: 0
+65  : 2

sabado0-17 : 018-65: 3+65  : 0domingo0-17 : 018-65: 0+65  : 6
      */




    }
}
