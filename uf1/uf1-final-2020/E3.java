import java.io.*;
import java.util.*;

public class E3 {

    public static void main(String[] args) {
    Scanner input = new Scanner(System.in);
    
    int N = input.nextInt(); //cantidad de barriles
    int [] barriles = new int [N];

    for (int i = 0; i < N; i++) {
        barriles [i] = input.nextInt(); //fuerza de los barriles
    }

    int DK = input.nextInt(); //posición inicial de DK
    int cont=0; //para controlar el bucle (= núm. de saltos)

    while (DK>=0 && DK<N){ //siempre que DK esté entre los barriles
        DK= DK+barriles [DK];
        //System.out.println(DK);

        cont++;
        if ( cont>N ){
            //si el número de saltos es mayor al número de saltos es que ha caído en bucle
            System.out.println("BUCLE");
            System.exit(0);
        }
    }
    
    //aver quepasa (?
    System.out.println(DK<0? "IZQUIERDA": "DERECHA");

    }
}
