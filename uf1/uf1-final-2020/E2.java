import java.io.*;
import java.util.*;

public class E2 {

    public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    float CPU, CPUMAX;
    int RAM, RAMMAX;
    String PC, PCMAX = "";

    CPU = input.nextFloat();
    RAM = input.nextInt();

    CPUMAX = CPU;
    RAMMAX = RAM;

    do {
        input.nextLine(); // Escapar el salto de línea que deja el int
        PC = input.nextLine();

        if (CPU>CPUMAX) {
          CPUMAX = CPU;
          RAMMAX = RAM;
          PCMAX = PC;
        } else if (CPU==CPUMAX) {
            if (RAMMAX>=RAM) {
              CPUMAX = CPU;
              RAMMAX = RAM;
              PCMAX = PC;
            }
        }

        CPU = input.nextFloat();
        RAM = input.nextInt();

      } while (CPU!=0 && RAM!=0);

      System.out.println(PCMAX+"\nCPU: "+CPUMAX+"\nRAM: "+RAMMAX);

    }
}
