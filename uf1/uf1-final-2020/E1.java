import java.io.*;
import java.util.*;

public class E1 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        char op = input.nextLine().charAt(0); //lee el símbolo
        int operando = input.nextInt(); //primer número
        int N = input.nextInt(); //cantidad de números

        int[] numeros = new int[N]; //array de todos los números en los que operar

        for (int i = 0; i < N; i++) {
            numeros[i]=input.nextInt();     //inputs del array
        }

        if (operando==0 && op=='/'){ //si la division es entre 0
            System.out.println("UNDEFINED");
            System.exit(0); //no llega a hacer el for
        }

        for (int i = 0; i < N; i++) { //operaciones
            if (op=='+') System.out.print(numeros[i]+operando+" ");
            if (op=='-') System.out.print(numeros[i]-operando+" ");
            if (op=='*') System.out.print(numeros[i]*operando+" ");
            if (op=='/') System.out.print(numeros[i]/operando+" ");
        }


    }
}
