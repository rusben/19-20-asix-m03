import java.io.*;
import java.util.*;

/*
  Leer un número mayor o igual a 0 y decir si es primo o no
  Imprimir los números primos del 0 al 100
*/

public class HelloPrimes {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int n = input.nextInt();
      int i = 2;
      boolean isprime = true;

      while (i < n) {

        // Mirar si n es divisible entre i
        if (n%i == 0) { // i es divisor de n, no es primo
          System.out.println("NO ES PRIMO");
          isprime = false;
          break;
        }

        i++;
      }

      // Si hemos acabado el bucle en busca de divisores y
      // no hemos encontrado ninguno, pues el número es primos
      // y el valor del booleano isprime no se habrá modificado.
      if (isprime) System.out.println("ES PRIMO");


    }
}
