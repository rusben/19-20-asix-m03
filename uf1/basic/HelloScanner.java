// Para utilizar el scanner (leer de teclado) necesitamos importar la librería
import java.util.Scanner;

public class HelloScanner {
   public static void main(String[] args) {

     int a,b;
     float f;
     double g;

     String s;

     // Definición del objeto Scanner que nos 'conectará' con el teclado
     Scanner keyboard = new Scanner(System.in);

     System.out.println("Escribe un número entero:");
     // Leemos el número entero
     a = keyboard.nextInt();
     System.out.println("Has escrito: "+a);

     System.out.println("Escribe un número entero:");
     // Leemos el número entero
     b = keyboard.nextInt();
     System.out.println("Has escrito: "+b);

     System.out.println("Escribe un número decimal:");
     // Leemos el número decimal
     f = keyboard.nextFloat();
     System.out.println("Has escrito: "+f);

     System.out.println("Escribe un número decimal:");
     // Leemos el número decimal
     g = keyboard.nextDouble();
     System.out.println("Has escrito: "+g);

    System.out.println("Escribe un texto:");
    // Leemos un texto
    s = keyboard.next();
    System.out.println("Has escrito: "+s);

    System.out.println("Cuidado! nextLine() puede fallar si antes se lee un entero!");
    // Leemos el número entero
    a = keyboard.nextInt();
    System.out.println("Has escrito: "+a);
    // Leemos un texto
    s = keyboard.nextLine();
    System.out.println("Has escrito: "+s);




   }
}
