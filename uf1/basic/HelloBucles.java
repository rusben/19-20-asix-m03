import java.io.*;
import java.util.*;

/*
  Imprimir de 0 a 100 con while, for y do-while
*/

public class HelloBucles {

    public static void main(String[] args) {

      System.out.println("while");

      int i = 0;
      while (i <= 100) {
        System.out.println(i);
        i++;
      }

      System.out.println("for");

      for (int j = 0; j <= 100; j++) {
        System.out.println(j);
      }

      System.out.println("do-while");

      int k = 0;
      do {
        System.out.println(k);
        k++;
      } while (k <= 100);

    }
}
