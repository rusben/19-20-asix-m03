import java.io.*;
import java.util.*;

/*
  Leer dos números n y m (n <= m) e imprimir
  los números primos entre n y m
*/

public class HelloPrimesN_M {

    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int n = input.nextInt();
      int m = input.nextInt();

      // Este bucle sirve para evaluar todos los números
      // entre n y m
      for (int i = n; i <= m; i++) {

        // Guardar el valor de i-1 en j para tratar
        // de saber si j es primo (el valor i es el propio número)
        boolean prime = true;
        int j = i - 1;
        // Comprovar si el número i es primo
        while (j > 1) {

           // Comprovamos si es primo
           if (i % j == 0) {
             prime = false;
             break;
           }
           j--;
        }

        // Al salir del bucle si no hemos encontrado ningún divisor
        // nadie habrá puesto el booleano prime a false,
        // entonces el número es primo
        if (prime) System.out.println(i);
      }
    }
}
