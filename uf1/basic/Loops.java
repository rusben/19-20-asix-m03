import java.io.*;
import java.util.*;

public class Loops {
    public static void main(String[] args) {

      Scanner input = new Scanner(System.in);

      int i, n, m, r;

      /* Leer de uno en uno */
      n = input.nextInt();
      while (n != -1) {
        System.out.println(n);
        n = input.nextInt();
      }

      n = input.nextInt();

      /* La entrada tiene almenos dos elementos */

      /* Leer de uno en uno evaluando dos elementos
      en cada vuelta del bucle */
      n = input.nextInt();
      m = input.nextInt();

      i = 0; /* iteración del bucle */
      while (m != -1) {
        System.out.println("Iteración: "+i);
        System.out.println(n);
        System.out.println(m);

        n = m;
        m = input.nextInt();
        i++;
      }



      /* El caso en el que la entrada no nos aseguran que
      tenga dos elementos */
      if (input.hasNext()) {
        m = input.nextInt();

        i = 0; /* iteración del bucle */
        while (m != -1) {
          System.out.println("Iteración: "+i);
          System.out.println(n);
          System.out.println(m);

          n = m;
          m = input.nextInt();
          i++;
        }

      } else {
        /* Aquí escribiríamos el código para el caso en el
        que la entrada tiene un elemento */
      }









    }
}
