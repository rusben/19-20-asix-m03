import java.io.*;
import java.util.*;

/*
  Imprimir de 0 a 100 con while, for y do-while
*/

public class HelloFear {

    public static void main(String[] args) {

      Scanner input = new Scanner (System.in);

      /* Leemos int hasta que aparezca un -1 */
      /* int n  = input.nextInt();
      while (n != -1) {
        System.out.println(n);
        n  = input.nextInt();
      }

      int n  = input.nextInt();
      for (;n != -1;) {
        System.out.println(n);
        n  = input.nextInt();
      } */

      /*
        int i=0;
        for (;i < n; ) {
          // imprime los numeros multiplos de 3
          //if (i%3==0){
          //   System.out.println(i);
          // }
          System.out.println(i);
          i+=2;
        }

        */

        /*
        Imprime sólo los números pares
        int j = 0;
        while (j < n) {
          System.out.println(j);
          j+=2;
        } */





    }

  }
