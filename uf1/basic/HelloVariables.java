public class HelloVariables {
   public static void main(String[] args) {

      // Definición de variables de diferentes tipos
      int a;
      int b;
      char c;
      double d;
      float f;
      boolean h;
      String s;
      String t;


      // Definición de variables y asignación
      String w = "Cadena de texto";
      int y = 11;
      float z = 22.3f;

      // Asignación de valores a las diferentes variables
      a = 3;
      b = 7;

      d = 7.0d;
      f = 44.3f;
      h = true;

      c = 'A'; // Esto es un carácter
      s = "Esto es una frase";

      // Operaciones
      d = a + b; // En un double se puede guardar un integer
      a = (int) f; // En cambio, si guardamos un tipo de datos en otro con menos precisión dará error. Hay que especificar la conversión.

      // Podemos concatenar cadenas de texto.
      t = w+" "+s;

      System.out.println(s+" este texto se añade a la cadena s");
      System.out.println(t);
   }
}
